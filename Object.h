#pragma once

#include <GL/glew.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <vector>
#include "Vertex.h"

class Object
{
public:
	Object() {}
	virtual ~Object() {}

	const GLfloat* getData() const { return reinterpret_cast<const GLfloat*>(vertices_.data()); }
	const GLuint* getIndices() const { return indices_.data(); }

	int getNVerts() const { return vertices_.size(); }
	int getNIndices() const { return indices_.size(); }

	glm::mat4 getModel() const { return scale_ * translate_ * rotate_; }

	void rotate(GLfloat angle, glm::vec3 axis) { rotate_ = glm::rotate(rotate_, glm::radians(angle), axis); }
	void move(GLfloat dx, GLfloat dy, GLfloat dz) { translate_ = glm::translate(translate_, glm::vec3(dx, dy, dz)); }
	void scale(GLfloat sx, GLfloat sy, GLfloat sz) { scale_ = glm::scale(scale_, glm::vec3(sx, sy, sz)); }

	void setPos(GLfloat x, GLfloat y, GLfloat z) { translate_ = glm::translate(glm::mat4(), glm::vec3(x, y, z)); }

protected:
	std::vector<Vertex> vertices_;
	std::vector<GLuint> indices_;

	glm::mat4 translate_;
	glm::mat4 rotate_;
	glm::mat4 scale_;
};

