#include "Cylinder.h"

#include <glm/gtc/matrix_transform.hpp>

Cylinder::Cylinder(GLfloat r, GLfloat w, GLuint nc, Color c)
	: radius_(r),
	  width_(w),
	  ncorners_(nc),
	  color_(c)
{
	vertices_ = std::vector<Vertex>(2 + 2 * ncorners_ + 2 * ncorners_);
	indices_ = std::vector<GLuint>(12 * ncorners_);

	const glm::vec3 UPPER_NORM(0.f, 0.f, 1.f);
	const glm::vec3 LOWER_NORM(0.f, 0.f, -1.f);

	//vertices_[0] = (0, 0, 0)
	vertices_[1] = Vertex(0.f, 0.f, width_);
	vertices_[0].setNorm(LOWER_NORM).setColor(color_);
	vertices_[1].setNorm(UPPER_NORM).setColor(color_);

	vertices_[2] = Vertex(0.f, radius_, 0.f);
	vertices_[2 + ncorners_] = Vertex(0.f, radius_, width_);
	vertices_[2].setNorm(LOWER_NORM).setColor(color_);
	vertices_[2 + ncorners_].setNorm(UPPER_NORM).setColor(color_);

	//VERTICES
	//a) main faces
	const Vertex& ORIGIN_LOWER = vertices_[2];
	const Vertex& ORIGIN_UPPER = vertices_[2 + ncorners_];

	int n = ncorners_ + 2;
	int offset = ncorners_;

	GLfloat angle = -360.f / ncorners_;
	const GLfloat increment = angle;

	int i = 3;

	for (; i < n; ++i)
	{
		vertices_[i] = ORIGIN_LOWER.clone().rotateXY(angle);
		vertices_[i + offset] = ORIGIN_UPPER.clone().rotateXY(angle);

		angle += increment;
	}

	//b) side faces
	n = vertices_.size();
	i = 2 * ncorners_ + 2;

	for (int j = 2; i < n; i += 2, ++j)
	{
		glm::vec3 facenorm = glm::normalize(vertices_[j].getVec3());

		vertices_[i] = vertices_[j].clone().setNorm(facenorm);
		vertices_[i + 1] = vertices_[j + offset].clone().setNorm(facenorm);
	}

	//INDICES
	//lower face; CW winding
	i = 3;
	n = ncorners_;
	int vnumber = 2;

	indices_[0] = 0;
	indices_[1] = ncorners_ + 1;
	indices_[2] = 2;

	for (int j = 1; j < n; ++j, i += 3)
	{
		indices_[i] = 0;
		indices_[i + 1] = vnumber;
		indices_[i + 2] = vnumber + 1;

		++vnumber;
	}

	//upper face; CCW winding
	indices_[i] = 1;
	indices_[i + 1] = ncorners_ + 2;
	indices_[i + 2] = 2 * ncorners_ + 1;

	i += 3;
	vnumber = ncorners_ + 2;

	for (int j = 1; j < n; ++j, i += 3)
	{
		indices_[i] = 1;
		indices_[i + 1] = vnumber + 1;
		indices_[i + 2] = vnumber;

		++vnumber;
	}

	//side faces; alternating winding
	n = ncorners_ - 1;
	vnumber = 2 * ncorners_ + 2;

	for (int j = 0; j < n; ++j, i += 6)
	{
		indices_[i] = vnumber;
		indices_[i + 1] = vnumber + 1;
		indices_[i + 2] = vnumber + 2;

		indices_[i + 3] = vnumber + 1;
		indices_[i + 4] = vnumber + 3;
		indices_[i + 5] = vnumber + 2;

		vnumber += 2;
	}

	//last side face
	n = vertices_.size() - 2;

	indices_[i] = n;
	indices_[i + 1] = n + 1;
	indices_[i + 2] = 2 * ncorners_ + 2;

	indices_[i + 3] = n + 1;
	indices_[i + 4] = 2 * ncorners_ + 3;
	indices_[i + 5] = 2 * ncorners_ + 2;
}