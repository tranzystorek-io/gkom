#include "StateMachine.h"

StateMachine::StateMachine(double period, State initial)
	: state_(initial),
	  t_(0.),
	  period_(period)
{
}

void StateMachine::update(double dt)
{
	t_ += dt;

	if (t_ > period_)
	{	
		state_ = (state_ == State::FORWARD) ? State::BACKWARD : State::FORWARD;
		t_ -= period_;
	}
}
