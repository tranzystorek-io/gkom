#pragma once
#include "Object.h"

class Cuboid : public Object
{
public:
	enum FaceDir {OUTWARDS, INWARDS};

public:
	Cuboid(GLfloat w, GLfloat h, GLfloat d, Color c, FaceDir fdir = FaceDir::INWARDS);

	GLfloat getW() const { return width_; }
	GLfloat getH() const { return height_; }
	GLfloat getD() const { return depth_; }

private:
	GLfloat width_;
	GLfloat height_;
	GLfloat depth_;

	Color color_;
};

