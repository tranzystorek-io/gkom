#pragma once

class Timer
{
public:
	Timer();

	void start();
	double restart();

	double elapsed() const;

private:
	double last_time_;
};

