#include "Timer.h"

#include <GLFW/glfw3.h>

Timer::Timer()
	: last_time_(0.)
{
}

void Timer::start()
{
	last_time_ = glfwGetTime();
}

double Timer::restart()
{
	double current = glfwGetTime();
	double ret = current - last_time_;
	last_time_ = current;

	return ret;
}

double Timer::elapsed() const
{
	return glfwGetTime() - last_time_;
}
